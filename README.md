# MEDLINE Loader #

MEDLINE Loader is a Java-based command-line utility that may be used to download the latest MEDLINE citations for biomedical literature from the [National Library of Medicine](http://www.nlm.nih.gov/), and to load those citations into a local relational database to facilitate advanced analysis. 

## Building MEDLINE Loader ##

Building the MEDLINE Loader is accomplished by running the following from the project's base directory:

    $ mvn package

## Preparation ##

Three preparatory steps must be taken before MEDLINE Loader may be used.  These are obtaining a lease from NLM to access MEDLINE data, creating the local database into which MEDLINE data will be ultimately stored, and ensuring your system has sufficient disk space available to store everything.

### 1. Obtaining a MEDLINE/PubMed Lease from NLM ###

In order for the MEDLINE Loader to download data files, you must first obtain a MEDLINE/PubMed lease from NLM.  These leases are tied to IP addresses.

See [this NLM page on Leasing Journal Citations](http://www.nlm.nih.gov/databases/journal.html) for details on how to obtain a MEDLINE/PubMed lease.

### 2. Create Local Database ###

Before the MEDLINE Loader can be used, the database into which MEDLINE citations and associated records will be stored must be created.  MEDLINE Loader is pre-configured to use MySQL as its database back-end.  If you don't already have it installed, you should first download and install the [MySQL Community Server](https://dev.mysql.com/downloads/mysql/).

Once MySQL is installed, run the following command from the project's base directory:

    $ mysql -uroot < createdb.sql

This will create the _medline_ database, with appropriate default values to integrate with the MEDLINE Loader's default settings.

### 3. Ensure You Have Sufficient Free Disk Space ###

The MEDLINE Loader's intermediate files and target database tables can take _hundreds of gigabytes_ of disk space.  Please be sure that you have at least **75 gigabytes** of disk space available before starting this process!

## Usage ##

To use the MEDLINE Loader and to see a list of command-line switches, run the program without any arguments:

    $ java -jar medline-loader-1.0.jar 
    MEDLINE Loader
    --------------
    Copyright 2015 The University of Vermont and State Agricultural College.  All rights reserved.
    
    usage: medline-loader [-d <string>] [-h <string>] --load | --prepare [-p <string>]  [-u <string>]
     -d,--db <string>     the database name (default 'medline')
     -h,--host <string>   the database host (default 'localhost')
        --load            only load previously-prepared files into the target database
     -p,--pass <string>   the database user password (default 'medline')
        --prepare         only prepare database files for import
     -u,--user <string>   the database user name (default 'medline')

### 1. Prepare MEDLINE Files For Import ###

The MEDLINE Loader has two primary modes of operation: `prepare` and `load`.  The preparation stage involves downloading MEDLINE files and transforming them such that they may be loaded into the target database.  The loading stage involves taking those prepared files and importing them into the target database.
 
To download and prepare files for import, run the following command:

    $ java -Xmx500m -jar medline-loader-1.0.jar --prepare
    MEDLINE Loader
    --------------
    Copyright 2015 The University of Vermont and State Agricultural College.  All rights reserved.
    
    INFO  Load - process started at Wed Jun 17 11:49:18 EDT 2015
    INFO  FTPClient - connected to 'ftp.nlm.nih.gov'
    INFO  FTPClient - disconnected from 'ftp.nlm.nih.gov'
    INFO  AbstractFTPLoader - [1]  start
    INFO  AbstractFTPLoader - [2]  start
    INFO  FTPClient - connected to 'ftp.nlm.nih.gov'
    INFO  AbstractFTPLoader - [2]  (1/779, 0%)  processing 'medline15n0001.xml.gz'
    INFO  FTPClient - connected to 'ftp.nlm.nih.gov'
    INFO  AbstractFTPLoader - [1]  (2/779, 0%)  processing 'medline15n0002.xml.gz'
    INFO  AbstractFTPLoader - [2]  (3/779, 0%)  processing 'medline15n0003.xml.gz'
    INFO  AbstractFTPLoader - [1]  (4/779, 0%)  processing 'medline15n0004.xml.gz'
    ...
    INFO  AbstractFTPLoader - [1]  (778/779, 99%)  processing 'medline15n0778.xml.gz'
    INFO  AbstractFTPLoader - [2]  (779/779, 100%)  processing 'medline15n0779.xml.gz'
    INFO  AbstractFTPLoader - [1]  done.
    INFO  FTPClient - disconnected from 'ftp.nlm.nih.gov'
    INFO  AbstractFTPLoader - [2]  done.
    INFO  FTPClient - disconnected from 'ftp.nlm.nih.gov'
    INFO  AbstractLoader - processing 779 files across 2 threads took 1 hour, 19 minutes, 44 seconds
    INFO  Load - preparing files finished at Thu Jun 18 17:03:36 EDT 2015 (took 1 hour, 19 minutes, 47 seconds).

Downloaded files will be stored in a temporary folder while they are being processed, and are deleted after processing is completed.  Resultant files that will be imported into the local database are stored in the _out_ folder in the current working directory:

    $ ls out
    author.txt	basic.txt	mesh.txt	pubtype.txt

Each of these files is associated with each of the four tables in the target database.

#### Prepare Stage is Resumable ####

During the preparation stage, hundreds of files are downloaded over potentially several hours.  It is possible that during this time, the process might be interrupted (perhaps by an `OutOfMemoryError`, lost network connection, user error, etc.).  Should any such interruption occur, it is important to know that the download and processing of data files may be resumed where it left off with an extremely remote chance of data corruption.

This is because metadata is generated about each successfully processed file (allowing the system to know where to resume operations), and because data generated from each downloaded file is processed independently, the results from which are appended to the master target files for database import only after all individual file processing has completed.

Corruption of the master files can occur **only** if the MEDLINE Loader is interrupted during the few milliseconds required to append individual file's data to its respective master file.  This is extremely unlikely to occur, as while the processing of an individual file may take many seconds, appending those data to the master file takes just a few milliseconds.

#### Important Note Regarding Memory and Performance ####

MEDLINE Loader is a multi-threaded process that can leverage the cores of your CPU to download and prepare MEDLINE files in parallel, which can dramatically improve performance.

When MEDLINE Loader executes its `prepare` function, it determines how much memory it has been allocated, and creates as many threads as it can safely use without negatively impacting other user and system processes.  MEDLINE Loader will use at least one core, but may use up to (_N_-2) cores, where _N_ is the total number of cores in your system's CPU.

MEDLINE Loader's preparation stage requires **200 megabytes of Java heap memory per thread** to properly execute, and may otherwise suffer severely degraded performance, or even fail with an `OutOfMemoryError`.  It is therefore suggested to allocate _at least 2.5 times_ the required memory per thread to the JVM, but more is better and will translate to improved performance, especially on systems with many CPU cores.

To allocate the minimum suggested memory to the MEDLINE Loader process, use the JVM option `-Xmx500m` as in the example above.
 
See [Oracle's Java SE Documentation](http://docs.oracle.com/javase/7/docs/technotes/tools/windows/java.html) for details about `-Xmx` and other JVM options.

### 2. Load Prepared Files Into Local Database ###

To load the prepared MEDLINE files into the local database, execute the following:

    $ java -jar medline-loader-1.0.jar --load
    MEDLINE Loader
    --------------
    Copyright 2015 The University of Vermont and State Agricultural College.  All rights reserved.
    
    INFO  Load - process started at Mon Jun 22 09:41:28 EDT 2015
    INFO  AbstractLoader - populating database from './out' -
    INFO  AbstractLoader -  loading './out/basic.txt' into table 'basic'
    INFO  DataSource - getConnection : establishing connection to 'medline'
    INFO  AbstractLoader -  building indexes for table 'basic'
    INFO  AbstractLoader -  loading './out/mesh.txt' into table 'mesh'
    INFO  AbstractLoader -  building indexes for table 'mesh'
    INFO  AbstractLoader -  loading './out/pubtype.txt' into table 'pubType'
    INFO  AbstractLoader -  building indexes for table 'pubType'
    INFO  AbstractLoader -  loading './out/author.txt' into table 'author'
    INFO  AbstractLoader -  building indexes for table 'author'
    INFO  AbstractLoader - finished populating database.  took 2 hours, 30 minutes, 15 seconds
    INFO  Load - populating database finished at Mon Jun 22 12:11:45 EDT 2015 (took 2 hours, 30 minutes, 16 seconds).
    INFO  DataSource - close : closing connection 'medline'


Note that as soon as the `load` process completes, you may safely delete intermediate database import files in the _out_ folder.

## License and Copyright ##

MEDLINE Loader is Copyright 2015 [The University of Vermont and State Agricultural College](https://www.uvm.edu/).  All rights reserved.

MEDLINE Loader is licensed under the terms of the [GNU General Public License (GPL) version 3](https://www.gnu.org/licenses/gpl-3.0-standalone.html).